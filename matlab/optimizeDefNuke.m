% Filename: optimizeDefNuke.m
% Description: Creates an optimized offensive nuke based on a defensive
% army input
% Created by: Cong Nguyen
% Created on: 28 Dec 2015

function [ troops ] = optimizeDefNuke(      recruitPermission, ...
    provisions, ...
    attTroops, ...
    wall, ...
    morale, ...
    officer, ...
    minTime, ...
    lowerBound, upperBound)
%OPTIMIZEATTACKERNUKE Optimizes def nuke based on attacking force
%   recruitPermission [sp sw ar hc treb]
%   attTroops [sp sw axe ar lc ma hc ram cat serk treb snob knight]
%   lowerBound [sp sw ar hc treb]
%   upperBound [sp sw ar hc treb]

global attStr;
global wallStr;
global moraleStr;
global officerStr;
attStr = '';
for i = 1:length(attTroops)
    if i ~= length(attTroops)
        attStr = strcat(attStr, int2str(attTroops(i)), ',');
    else
        attStr = strcat(attStr, int2str(attTroops(i)));
    end
end
% disp(attStr);

wallStr = int2str(wall);
moraleStr = int2str(morale-5);
officerStr = int2str(officer);

spearInfo = Unit('spear', 10, 25, 45, 10, 50, 30, 20, 1, 1.5, 0);
swordInfo = Unit('sword', 25, 55, 5, 30, 30, 30, 70, 1, 2, 0);
axeInfo = Unit('axe', 45, 10, 5, 10, 60, 30, 40, 1, 2.5, 0);
archerInfo = Unit('archer', 25, 10, 30, 60, 80, 30, 60, 1, 3, 0);
lcInfo = Unit('light calvalry', 130, 30, 40, 30, 125, 100, 250, 4, 6, 0);
maInfo = Unit('mounted archer', 140, 40, 30, 50, 250, 200, 100, 5, 7.5, 0);
hcInfo = Unit('heavy calvalry', 150, 200, 160, 180, 200, 150, 600, 6, 10, 0);
ramInfo = Unit('ram', 2, 20, 50, 20, 300, 200, 200, 5, 8, 0);
catInfo = Unit('catapult', 100, 100, 50, 100, 320, 400, 100, 8, 7.5, 0);
serkInfo = Unit('berserker', 300, 100, 100, 50, 1200, 1200, 2400, 6, 20, 0);
trebInfo = Unit('trebutchet', 30, 200, 250, 200, 4000, 2000, 1000, 10, 20, 0);

% max provision constraint, A*x' = b
Aeq_att = [spearInfo.provision swordInfo.provision archerInfo.provision hcInfo.provision trebInfo.provision];
beq_att = provisions;
% display options
%     options = fminimax('fgoalattain','Display','off');

if nargin < 8
    %     disp('nargin < 7');
    lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    ub(recruitPermission ~= 0) = provisions; % upper bound
    ub(recruitPermission == 0) = 0;
elseif nargin < 9
    %     disp('nargin < 8');
    if length(lowerBound) == 0
        lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    else
        lb = double(lowerBound);
    end
    ub(recruitPermission ~= 0) = provisions; % upper bound
    ub(recruitPermission == 0) = 0;
else
    if length(lowerBound) == 0
        lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    else
        lb = double(lowerBound);
    end
    
    if length(upperBound) == 0
        ub(recruitPermission ~= 0) = provisions; % upper bound
        ub(recruitPermission == 0) = 0;
    else
        ub = double(upperBound);
    end
end

if minTime == 1
   x0 = lb; 
else 
   x0 = ub;
end
%     troops = 0;
% goal = all zeros, equal weights
options = optimoptions('fminimax','Display','off');
troops = fminimax(@simResult,x0,[],[],Aeq_att,beq_att,lb,ub, [], options);
troops = round(troops);
sp = troops(1);
sw = troops(2);
ar = troops(3);
hc = troops(4);
treb = troops(5);

% display sim result
fprintf('\nSP: %d, SW: %d, AR: %d, HC: %d, treb: %d\n',sp,sw,ar,hc,treb);
fprintf('recruit time: %.1f days\n',round(rt(troops)/60/24*10)./10);
[w,c,i] = resDef(troops);
fprintf('wood: %d, clay: %d, iron: %d\n\n',w,c,i);

% generate at sim string
defStr = [int2str(sp),',',int2str(sw),',0,',int2str(ar),',0,0,',int2str(hc),',0,0,0,',int2str(treb),',0,0'];
url1 = 'http://rampadc.github.io/TW2-Sim/tw2-sim.html?att=';
url2 = '&def=';
url3 = '&wall=';
url4 = '&moral=';
url5 = '&officer=';
url6 = '&luck=0&attChurch=1&defChurch=1';
fullUrl = strcat(url1,attStr,url2,defStr,url3,wallStr,url4,moraleStr,url5,officerStr,url6);
% disp(fullUrl);
web(fullUrl);
end

function f = simResult(troops)
% troops = [sp sw ar hc treb]
global attStr;
global wallStr;
global moraleStr;
global officerStr;

sp = troops(1);
sw = troops(2);
ar = troops(3);
hc = troops(4);
treb = troops(5);

simDefStr = [int2str(sp),',',int2str(sw),',0,',int2str(ar),',0,0,',int2str(hc),',0,0,0,',int2str(treb),',0,0'];
% disp(simDefStr);
% url1 = 'http://localhost:63342/MatlabNodeSim/sim.html?att=';
url1 = 'http://127.0.0.1:1357/?att=';
url2 = '&def=';
url3 = '&wall=';
url4 = '&moral=';
url5 = '&officer=';
url6 = '&luck=0&attChurch=1&defChurch=1';

fullUrl = strcat(url1,attStr,url2,simDefStr,url3,wallStr,url4,moraleStr,url5,officerStr,url6);
% disp(fullUrl);
% feed this troops # into the simulator
result = urlread(fullUrl);

c = textscan(result, '%d %d %d', 'Delimiter', ','); % attackerRemaining, defLosses, wallChange = wallBefore-wallAfter=0 = no change

attLoss = c{1}'; defLeft = c{2}'; wallChange = c{3}';

al = zeros(13); dl = zeros(5);
% convert to double
wallChange = double(wallChange(1));
for i=1:13
    al(i) = double(attLoss(i));
    dl(i) = double(defLeft(i));
end

f = [al(1) al(2) al(3) al(4) al(5) al(6) al(7) al(8) al(9) al(10) al(11) al(12) al(13) ...
    dl(1) dl(2) dl(3) dl(4) dl(5) ...
    wallChange
    ];
end

function time = rt(troops)
% troops: [sp sw ar hc treb]
spearInfo = Unit('spear', 10, 25, 45, 10, 50, 30, 20, 1, 1.5, 0);
swordInfo = Unit('sword', 25, 55, 5, 30, 30, 30, 70, 1, 2, 0);
archerInfo = Unit('archer', 25, 10, 30, 60, 80, 30, 60, 1, 3, 0);
hcInfo = Unit('heavy calvalry', 150, 200, 160, 180, 200, 150, 600, 6, 10, 0);
trebInfo = Unit('trebutchet', 30, 200, 250, 200, 4000, 2000, 1000, 10, 20, 0);

time = troops(1)*spearInfo.recruitmentTime + troops(2)*swordInfo.recruitmentTime + troops(3)*archerInfo.recruitmentTime + troops(4)*hcInfo.recruitmentTime + troops(5)*trebInfo.recruitmentTime;
end

function [wood, clay, iron] = resDef(troops)
% troops: [sp sw ar hc treb]
spearInfo = Unit('spear', 10, 25, 45, 10, 50, 30, 20, 1, 1.5, 0);
swordInfo = Unit('sword', 25, 55, 5, 30, 30, 30, 70, 1, 2, 0);
archerInfo = Unit('archer', 25, 10, 30, 60, 80, 30, 60, 1, 3, 0);
hcInfo = Unit('heavy calvalry', 150, 200, 160, 180, 200, 150, 600, 6, 10, 0);
trebInfo = Unit('trebutchet', 30, 200, 250, 200, 4000, 2000, 1000, 10, 20, 0);

wood = double(troops(1)*spearInfo.timberCost + troops(2)*swordInfo.timberCost + troops(3)*archerInfo.timberCost + troops(4)*hcInfo.timberCost + troops(5)*trebInfo.timberCost);
clay = double(troops(1)*spearInfo.clayCost + troops(2)*swordInfo.clayCost + troops(3)*archerInfo.clayCost + troops(4)*hcInfo.clayCost + troops(5)*trebInfo.clayCost);
iron = double(troops(1)*spearInfo.ironCost + troops(2)*swordInfo.ironCost + troops(3)*archerInfo.ironCost + troops(4)*hcInfo.ironCost + troops(5)*trebInfo.ironCost);
end
