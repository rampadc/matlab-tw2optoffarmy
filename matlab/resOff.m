function [wood, clay, iron] = resOff(troops)
% offTroops: [axe lc ma ram serk]
    axeInfo = Unit('axe', 45, 10, 5, 10, 60, 30, 40, 1, 3+33/60, 0);
    lcInfo = Unit('light calvalry', 130, 30, 40, 30, 125, 100, 250, 4, 5+40/60, 0);
    maInfo = Unit('mounted archer', 140, 40, 30, 50, 250, 200, 100, 5, 6+28/60, 0);
    serkInfo = Unit('berserker', 300, 100, 100, 50, 1200, 1200, 2400, 6, 30, 0);
    ramInfo = Unit('ram', 2, 20, 50, 20, 300, 200, 200, 5, 12+57/60, 0);
    
    wood = double(troops(1)*axeInfo.timberCost + troops(2)*lcInfo.timberCost + troops(3)*maInfo.timberCost + troops(4)*ramInfo.timberCost + troops(5)*serkInfo.timberCost);
    clay = double(troops(1)*axeInfo.clayCost + troops(2)*lcInfo.clayCost + troops(3)*maInfo.clayCost + troops(4)*ramInfo.clayCost + troops(5)*serkInfo.clayCost);
    iron = double(troops(1)*axeInfo.ironCost + troops(2)*lcInfo.ironCost + troops(3)*maInfo.ironCost + troops(4)*ramInfo.ironCost + troops(5)*serkInfo.ironCost);
end