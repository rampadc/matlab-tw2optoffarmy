% Filename: optimizeAttackerNuke.m
% Description: Creates an optimized offensive nuke based on a defensive
% army input while consuming the least resources possible
% Created by: Cong Nguyen
% Created on: 13 July 2015

function [ troops ] = optimizeAttackerNukeRes( recruitPermission, ...
                                            attProvisions, ...
                                            defTroops, ...
                                            wall, ...
                                            morale, ...
                                            officer, ...
                                            lowerBound, upperBound)
%OPTIMIZEATTACKERNUKERES Optimizes attacking nuke based on defensive force
% while consumes the least resources possible
%
%   recruitPermission [axe lc ma ram serk]
%   defTroops [sp sw axe ar lc ma hc ram cat serk treb snob knight]
%   lowerBound [axe lc ma ram serk]
%   upperBound [axe lc ma ram serk]

global defStr;
global wallStr;
global moraleStr;
global officerStr;
defStr = '';
for i = 1:length(defTroops)
    if i ~= length(defTroops)
        defStr = strcat(defStr, int2str(defTroops(i)), ',');
    else
        defStr = strcat(defStr, int2str(defTroops(i)));
    end
end
%     disp(defStr);
wallStr = int2str(wall);
moraleStr = int2str(morale-10);
officerStr = int2str(officer);

spearInfo = Unit('spear', 10, 25, 45, 10, 50, 30, 20, 1, 1.5, 0);
swordInfo = Unit('sword', 25, 55, 5, 30, 30, 30, 70, 1, 2, 0);
axeInfo = Unit('axe', 45, 10, 5, 10, 60, 30, 40, 1, 2.5, 0);
archerInfo = Unit('archer', 25, 10, 30, 60, 80, 30, 60, 1, 3, 0);
lcInfo = Unit('light calvalry', 130, 30, 40, 30, 125, 100, 250, 4, 6, 0);
maInfo = Unit('mounted archer', 140, 40, 30, 50, 250, 200, 100, 5, 7.5, 0);
hcInfo = Unit('heavy calvalry', 150, 200, 160, 180, 200, 150, 600, 6, 10, 0);
ramInfo = Unit('ram', 2, 20, 50, 20, 300, 200, 200, 5, 8, 0);
catInfo = Unit('catapult', 100, 100, 50, 100, 320, 400, 100, 8, 7.5, 0);
serkInfo = Unit('berserker', 300, 100, 100, 50, 1200, 1200, 2400, 6, 20, 0);
trebInfo = Unit('trebutchet', 30, 200, 250, 200, 4000, 2000, 1000, 10, 20, 0);

% max provision constraint, A*x' = b
Aeq_att = [axeInfo.provision lcInfo.provision maInfo.provision ramInfo.provision serkInfo.provision];
beq_att = attProvisions;
% display options
%     options = fminimax('fgoalattain','Display','off');

if nargin < 7
%     disp('nargin < 7');
    lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    ub(recruitPermission ~= 0) = attProvisions; % upper bound
    ub(recruitPermission == 0) = 0;
elseif nargin < 8
%     disp('nargin < 8');
    if length(lowerBound) == 0
        lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    else
        lb = double(lowerBound);
    end
    ub(recruitPermission ~= 0) = attProvisions; % upper bound
    ub(recruitPermission == 0) = 0;
else
    if length(lowerBound) == 0
        lb = [0.0 0.0 0.0 0.0 0.0]; % lower bound
    else
        lb = double(lowerBound);
    end
    
    if length(upperBound) == 0
        ub(recruitPermission ~= 0) = attProvisions; % upper bound
        ub(recruitPermission == 0) = 0;
    else
        ub = double(upperBound);
    end
end

x0 = lb;
%     troops = 0;
% goal = all zeros, equal weights
options = optimoptions('fminimax','Display','off');
troops = fminimax(@simResult,x0,[],[],Aeq_att,beq_att,lb,ub, [], options);
troops = round(troops);
axe = troops(1);
lc = troops(2);
ma = troops(3);
ram = troops(4);
serk = troops(5);

% display sim result
fprintf('\naxe: %d, lc: %d, ma: %d, ram: %d, serk: %d\n',axe,lc,ma,ram,serk);
fprintf('recruit time: %.1f days\n',round(rt(troops)/60/24*10)./10);
[w,c,i] = resOff(troops);
fprintf('wood: %d, clay: %d, iron: %d\n\n',w,c,i);

% generate at sim string
attStr = ['0,0,',int2str(axe),',0,',int2str(lc),',',int2str(ma),',0,',int2str(ram),',0,',int2str(serk),',0,0,0'];
url1 = 'http://rampadc.github.io/TW2-Sim/tw2-sim.html?att=';
url2 = '&def=';
url3 = '&wall=';
url4 = '&moral=';
url5 = '&officer=';
url6 = '&luck=0&attChurch=1&defChurch=1';
fullUrl = strcat(url1,attStr,url2,defStr,url3,wallStr,url4,moraleStr,url5,officerStr,url6);
% disp(fullUrl);
web(fullUrl);
end

function f = simResult(troops)
global defStr;
global wallStr;
global moraleStr;
global officerStr;

attStr = ['0,0,',int2str(troops(1)),',0,',int2str(troops(2)),',',int2str(troops(3)),',0,',int2str(troops(4)),',0,',int2str(troops(5)),',0,0,0'];

url1 = 'http://127.0.0.1:1357/?att=';
url2 = '&def=';
url3 = '&wall=';
url4 = '&moral=';
url5 = '&officer=';
url6 = '&luck=0&attChurch=1&defChurch=1';

fullUrl = strcat(url1,attStr,url2,defStr,url3,wallStr,url4,moraleStr,url5,officerStr,url6);
% feed this troops # into the simulator
result = urlread(fullUrl);
%     disp(fullUrl);
c = textscan(result, '%d %d %d', 'Delimiter', ','); % attackerLost, defRemaining, wallAfter

attLoss = c{1}'; defLeft = c{2}'; wallAfter = c{3}';

al = zeros(5); dl = zeros(13);
% convert to double
wallAfter = double(wallAfter(1));
for i=1:13
    al(i) = double(attLoss(i));
    dl(i) = double(defLeft(i));
end

% with wall=0 as a goal
%     f = [dl(1) dl(2) dl(3) dl(4) dl(5) dl(6) dl(7) dl(8) dl(9) dl(10) dl(11) dl(12) dl(13) ...
%          wallAfter ...
%          al(1) al(2) al(3) al(4) al(5)
%     ];

% wall basher as goal
%     f = [wallAfter ...
%          al(1) al(2) al(3) al(4) al(5)
%     ];

% without wall=0 as a goal
% f = [dl(1) dl(2) dl(3) dl(4) dl(5) dl(6) dl(7) dl(8) dl(9) dl(10) dl(11) dl(12) dl(13) ...
%     al(1) al(2) al(3) al(4) al(5)
%     ];

% min resources, wall basher
    [w, c, i] = resOff(troops);
    totalRes = w + c + i;
    f = [wallAfter totalRes ];
end

function time = rt(troops)
% offTroops: [axe lc ma ram serk]
spearInfo = Unit('spear', 10, 25, 45, 10, 50, 30, 20, 1, 1.5, 0);
swordInfo = Unit('sword', 25, 55, 5, 30, 30, 30, 70, 1, 2, 0);
axeInfo = Unit('axe', 45, 10, 5, 10, 60, 30, 40, 1, 2.5, 0);
archerInfo = Unit('archer', 25, 10, 30, 60, 80, 30, 60, 1, 3, 0);
lcInfo = Unit('light calvalry', 130, 30, 40, 30, 125, 100, 250, 4, 6, 0);
maInfo = Unit('mounted archer', 140, 40, 30, 50, 250, 200, 100, 5, 7.5, 0);
hcInfo = Unit('heavy calvalry', 150, 200, 160, 180, 200, 150, 600, 6, 10, 0);
ramInfo = Unit('ram', 2, 20, 50, 20, 300, 200, 200, 5, 8, 0);
catInfo = Unit('catapult', 100, 100, 50, 100, 320, 400, 100, 8, 7.5, 0);
serkInfo = Unit('berserker', 300, 100, 100, 50, 1200, 1200, 2400, 6, 20, 0);
trebInfo = Unit('trebutchet', 30, 200, 250, 200, 4000, 2000, 1000, 10, 20, 0);

time = max(...
    troops(1)*axeInfo.recruitmentTime + troops(2)*lcInfo.recruitmentTime + troops(3)*maInfo.recruitmentTime + ...
    troops(4)*ramInfo.recruitmentTime, ... % hard coded number of rams
    troops(5)*serkInfo.recruitmentTime);
end

function [wood, clay, iron] = resOff(troops)
% offTroops: [axe lc ma ram serk]
    axeInfo = Unit('axe', 45, 10, 5, 10, 60, 30, 40, 1, 3+33/60, 0);
    lcInfo = Unit('light calvalry', 130, 30, 40, 30, 125, 100, 250, 4, 5+40/60, 0);
    maInfo = Unit('mounted archer', 140, 40, 30, 50, 250, 200, 100, 5, 6+28/60, 0);
    serkInfo = Unit('berserker', 300, 100, 100, 50, 1200, 1200, 2400, 6, 30, 0);
    ramInfo = Unit('ram', 2, 20, 50, 20, 300, 200, 200, 5, 12+57/60, 0);
    
    wood = double(troops(1)*axeInfo.timberCost + troops(2)*lcInfo.timberCost + troops(3)*maInfo.timberCost + troops(4)*ramInfo.timberCost + troops(5)*serkInfo.timberCost);
    clay = double(troops(1)*axeInfo.clayCost + troops(2)*lcInfo.clayCost + troops(3)*maInfo.clayCost + troops(4)*ramInfo.clayCost + troops(5)*serkInfo.clayCost);
    iron = double(troops(1)*axeInfo.ironCost + troops(2)*lcInfo.ironCost + troops(3)*maInfo.ironCost + troops(4)*ramInfo.ironCost + troops(5)*serkInfo.ironCost);
end
