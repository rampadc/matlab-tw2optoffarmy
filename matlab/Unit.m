classdef Unit
    %UNIT Unit in Tribal Wars 2
    
    properties
        name
        combatStrength
        infantryDefense
        calvalryDefense
        archersDefense
        timberCost
        clayCost
        ironCost
        provision
        recruitmentTime % in minutes
        isBeserker % set to 1 for beserkers
    end
    
    methods
        function obj = Unit(...
                name, combatStrength, ...
                infantryDefense, calvaryDefense, archersDefense, ...
                wood, clay, iron, farm, ...
                recruitmentTime, ...
                isBeserker ...
            )
            obj.name = name;
            obj.combatStrength = combatStrength;
            obj.infantryDefense = infantryDefense;
            obj.calvalryDefense = calvaryDefense;
            obj.archersDefense = archersDefense;
            obj.timberCost = wood;
            obj.clayCost = clay;
            obj.ironCost = iron;
            obj.provision = farm;
            obj.recruitmentTime = recruitmentTime;
            obj.isBeserker = isBeserker;
        end

        function strength = getAttackStrength(unit, crowded)
           if crowded && unit.isBeserker
               strength = unit.combatStrength * 2;
           else 
               strength = unit.combatStrength;
           end
        end
    end
    
end

