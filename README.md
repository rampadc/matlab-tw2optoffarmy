# README #
#### Quick summary 
This project is a MATLAB optimization tool developed to output an optimized offensive army to crush the input defending army.
#### Tools 
##### MATLAB 
[Matlab](http://au.mathworks.com/products/matlab/) is used to produce the optimized armies using its multiobjective optimzation add-on. If you've got a university's e-mail address, you could get MATLAB for 99 USD (being a student or academic).
##### Node 
[Node](https://nodejs.org/en/) is used to generate the static HTML file that outputs sim results with attacking, defending armies parameters inputed by MATLAB. 
##### Scripts 
+ **matlab/optimizeAttackerNuke**: a function to generate an offensive army to defeat an input defending army.
+ **matlab/optimizeAttackerNukeRes**: same as above but the offensive army generated will use as little resources as possible.
+ **node/matlabSimOff.js**: script to be run with nodeJS for offensive army optimization.
+ **node/matlabSimDef.js**: script to be run with nodeJS for defensive army optimization.

MATLAB R2014a was used to developed the scripts.
#### Usage
1. Install MATLAB and NodeJS. Add 'node' to the OS's path.
2. Open up the command tool (cmd on Windows, terminal on OS X), navigate to the repository's node/ folder, run **node matlabSimOff.js** or **node matlabSimDef.js**.
3. Open up MATLAB, change the working directory to the repository's matlab/ folder, input desired parameters into the functions as you need. See the function's help for more info.

[Usage Example](https://youtu.be/pJRDKCmP8bs)

#### MATLAB Functions Information
While having different functionality, both functions share the same interface. Only the syntax for `optimizeAttackerNuke` will be shown as `optimizeAttackerNukeRes` has the same syntax.

`optimizeDefNuke` has some different parameters which will be explained below.
##### Syntax 
+ ` [troops] = optimizeAttackerNuke(recruitPermissions, provisions, defTroops, wall, morale, officer)` produces an optimized army without any specified lower boundaries (reset at 0) or upper boundaries
+ ` [troops] = optimizeAttackerNuke(recruitPermissions, provisions, defTroops, wall, morale, officer, offensiveArmyLowerBounds)` produces an optimized army with the specified lower boundaries
+ ` [troops] = optimizeAttackerNuke(recruitPermissions, provisions, defTroops, wall, morale, officer, offensiveArmyLowerBounds, offensiveArmyUpperBounds)` produces an optimized army with the specified lower and upper boundaries.
+  ` [troops] = optimizeDefNuke(recruitPermissions, provisions, attTroops, wall, morale, officer, minTime, defLowerBounds, defUpperBounds)` has similar syntax to above functions with the exception of the ***minTime*** parameter.

The returned array *troops* is optional when using the function as the function will be producing formatted results as well as a URL to TW2-Sim.
##### Parameters 
+ `recruitPermissions`: an array of 0 and 1 to specify which units can be included in the optimized army. It must follows the following template  [axe lc ma ram serk] for offensive army optimization and [sp sw ar hc treb] for defensive army optimization .
+ `provisions`: provisions of the attacker or defender
+ `defTroops`: an array of defending troops. It must follows the following template: [sp sw axe ar lc ma hc ram cat serk treb snob knight]
+ `wall`: wall level
+ `morale`: morale of the attacking army as a percentage, eg: 100, 120, 150, etc. Because at a morale of 100, the TW2-Sim overshoots the attacking strength a bit, a reduction of 10% is added to the morale automatically.
+ `officer`: 1 if the attacker is using an officer (grandmaster), 0 if not
+ `minTime`: (def opt only) minimize recruitment time
+ `lowerBound` and `upperBound`: an array of the number of bounds for the attacking army. It must follows the template of [axe lc ma ram serk]

##### Examples 
1. Optimize army at 20k provisions, attacking a defending army with 1000 spears, 1000 swords, 1000 archers and 1000 HC with axe, lc, ma, ram, serk without any boundaries without officer and a morale of 100: `optimizeAttackerNuke([1 1 1 1 1], 20000, [1000 1000 0 1000 0 0 1000 0 0 0 0 0 0], 20, 100, 0)`.
2. Optimize army at 20k provisions, attacking a defending army with 1500 spears, 1000 swords, 1000 archers and 1000 HC with only axe, ram and serk with a maximum of 250 rams and a maximum of 1000 serks with grandmaster and a morale of 150: `optimizeAttackerNuke([1 0 0 1 1], 20000, [1500 1000 0 1000 0 0 1000 0 0 0 0 0 0], 20, 150, 1, [0 0 0 0 0], [20000 0 0 250 1000])`.
3. Optimize army at 20k provisions attacking a defending army with 1500 spears, 1000 swords, 1000 archers and 1000 HC with only lc and rams with grandmaster and a morale of 100 **and minimize resources usage**: `optimizeAttackerNukeRes([0 1 0 1 0], 20000, [1500 1000 0 1000 0 0 1000 0 0 0 0 0 0], 20, 100, 1)`.
4. Optimize a defending army against 12500 axes, 300 rams, 1000 serks with swords and HC only, does not look for minimum recruit time: `optimizeDefNuke([0 1 0 1 0], 20000, [0 0 12500 0 0 0 0 300 0 1000 0 0 0], 20, 100, 1, 0)`

### Note
Trebuchet calculations might not be correct as there is no actual mechanism implementation inside TW2-Sim.